import csv
import data_pb2 as data
import base64
from google.protobuf.timestamp_pb2 import Timestamp

def get_winner(user_action,agent_action):
	# come into this routine with ints
	if user_action == data.ROCK and agent_action == data.SCISSOR:
		return data.USER_WON
	elif user_action == data.ROCK and agent_action == data.PAPER:
		return data.AGENT_WON
	elif user_action == data.PAPER and agent_action == data.SCISSOR:
		return data.AGENT_WON
	elif user_action == data.PAPER and agent_action == data.ROCK:
		return data.USER_WON
	elif user_action == data.SCISSOR and agent_action == data.ROCK:
		return data.AGENT_WON
	elif user_action == data.SCISSOR and agent_action == data.PAPER:
		return data.USER_WON
	else:
		return data.DRAW


def get_winner_non_data_pb2(user_action,agent_action):
	# come into this routine with ints
	if user_action == 'ROCK' and agent_action == 'SCISSORS':
		return 'USER_WON'
	elif user_action == 'ROCK' and agent_action == 'PAPER':
		return 'AGENT_WON'
	elif user_action == 'PAPER' and agent_action == 'SCISSORS':
		return 'AGENT_WON'
	elif user_action == 'PAPER' and agent_action == 'ROCK':
		return 'USER_WON'
	elif user_action == 'SCISSORS' and agent_action == 'ROCK':
		return 'AGENT_WON'
	elif user_action == 'SCISSORS' and agent_action == 'PAPER':
		return 'USER_WON'
	else:
		return 'DRAW'
