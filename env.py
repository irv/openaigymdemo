import cog_settings
from data_pb2 import Observation #, ObservationDelta

from cogment import Environment, GrpcServer
import gym
import video
import pickle
import traceback
import datetime
import random
import sys
import os
import shutil
import subprocess


from threading import Thread, Lock

mutex = Lock()

HUMAN = 1
AGENT = 0

GAMMA = 0.95  
LEARNING_RATE = 0.001  
EXPLORATION_MAX = 1.0 
EXPLORATION_MIN = 0.01
EXPLORATION_DECAY = 0.995  


class Env(Environment):
    VERSIONS = {"env": "1.0.0"}

    def start(self, config):
        global my_gym_env
        try:
            mutex.acquire()

            # Section AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
            self.counter = 0

            print(f"environment starting: {config.env_name}")
            self.stream = video.StreamWriter()

            self.gym_env = gym.make(config.env_name)

            self.env_state = Observation()

            self.env_state.info.observation_space = self.gym_env.observation_space.shape[0]
            self.env_state.info.action_space = self.gym_env.action_space.n
            self.env_state.info.max_episode_steps = self.gym_env._max_episode_steps
            self.env_state.info.environment = config.env_name

            self.env_state.score = 0

            self.env_state.hyperparam.gamma = GAMMA
            self.env_state.hyperparam.learning_rate = LEARNING_RATE
            self.env_state.hyperparam.exploration_max = EXPLORATION_MAX
            self.env_state.hyperparam.exploration_min = EXPLORATION_MIN
            self.env_state.hyperparam.exploration_decay = EXPLORATION_DECAY

            self.env_state.trial_name = 'null'

            self.env_state.env_2_agent = 'do_B'
            self.env_state.env_2_human = 'A_B_done'

            # for now  state = self.gym_env.reset()
            # for now  image = self.gym_env.render(mode='rgb_array')
            # for now observation = Observation(
            #    env_name=config.env_name,
            #    image_width=image.shape[0],
            #    image_height=image.shape[1],
            #    pickled_action_space=pickle.dumps(self.gym_env.action_space),
            #    pickled_state=pickle.dumps(state),
            #    image=self.stream.compress(image)
            #)

            state = self.gym_env.reset()
            image = self.gym_env.render(mode='rgb_array')
            self.env_state.env_name = config.env_name
            self.env_state.image_width = image.shape[1]
            self.env_state.image_height = image.shape[0]
            self.env_state.pickled_action_space = pickle.dumps(self.gym_env.action_space)
            self.env_state.pickled_state = pickle.dumps(state)
            self.env_state.image = self.stream.compress(image)

            # for now return observation
            self.game = 0


            return self.env_state





        except:
            traceback.print_exc()
            raise
        finally:
            mutex.release()



    def update(self, actions):
        try:
            mutex.acquire()

            # CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
            #if pickle.loads(actions.participant[HUMAN].value) == 'do_C':
            #if pickle.loads(actions.human[0].value)[0] == 'X':
            if actions.human[0].value[0] == 'X':

                #self.env_state.trial_name = pickle.loads(actions.human[0].value)[1:]
                self.env_state.trial_name = actions.human[0].value[1:]

                if actions.human[0].value[1:] == "delcsv":
                    if os.path.isfile("./metrics/data/test.csv"):
                        os.remove("./metrics/data/test.csv")

                if actions.human[0].value[1:] == "dellog":
                    if os.path.isfile("./data.log"):
                        os.remove("./data.log")


                if actions.human[0].value[1:] == "excsv":
                    if os.path.isfile("./metrics/data/test.csv"):
                        os.remove("./metrics/data/test.csv")
                    shutil.copy("./metrics/data/good_example.csv","./metrics/data/test.csv")

                if actions.human[0].value[1:] == "log2csv":
                    pass






                self.game += 1   # increment game number
                self.env_state.score = 0
                observation = self.gym_env.reset()
                state = observation
                image = self.gym_env.render(mode='rgb_array')
                self.env_state.image_width = image.shape[1]
                self.env_state.image_height = image.shape[0]
                self.env_state.pickled_state = pickle.dumps(state)
                self.env_state.image = self.stream.compress(image)

                del self.env_state.observation[:]
                self.env_state.observation.extend(observation)
                self.env_state.done = False

                self.env_state.env_2_agent = 'do_D'
                self.env_state.env_2_human = 'C_D_done'

                return self.env_state


            # EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE
            #elif pickle.loads(actions.participant[HUMAN].value) == 'do_E':
            #elif pickle.loads(actions.human[0].value) == 'do_E':
            elif actions.human[0].value == 'do_E':

                image = self.gym_env.render(mode='rgb_array')
                self.env_state.image = self.stream.compress(image)
                #self.env_state.pickled_state = pickle.dumps(self.env_state.observation)
                self.env_state.image_width = image.shape[1]
                self.env_state.image_height = image.shape[0]

                try:
                    self.observation, self.reward, self.done, self.info = self.gym_env.step(pickle.loads(actions.agent[0].pickled_action))
                except:
                    print('problem with gym')
                    raise
                del self.env_state.observation[:]
                self.env_state.observation.extend(self.observation)
                self.env_state.done = self.done
                if not self.done:
                    self.env_state.score += 1
                # do reward here!!!
                #reward = reward if not done else -reward
                self.env_state.reward = self.reward if not self.done else -self.reward

                self.env_state.env_2_agent = 'do_F'
                self.env_state.env_2_human = 'E_F_done'
                return self.env_state

            #elif pickle.loads(actions.human[0].value) == 'do_nothing':
            elif actions.human[0].value == 'do_nothing':
                self.env_state.env_2_agent = 'do_nothing'
                self.env_state.env_2_human = 'A_B_done'
                return self.env_state

            #elif pickle.loads(actions.human[0].value) == 'do_envclose':
            elif actions.human[0].value == 'do_envclose':
                self.env_state.env_2_agent = 'do_nothing'
                self.env_state.env_2_human = 'A_B_done'
                self.gym_env.close()

                return self.env_state




            else:
                #human_response = pickle.loads(actions.human[0].value).split(":")
                human_response = actions.human[0].value.split(":")
                if human_response[0] == 'params':
                    #print("PPPPPPPPPPP params:",human_response[1],human_response[2])
                    self.env_state.env_2_agent = 'do_nothing'
                    self.env_state.env_2_human = 'A_B_done'
                    param,value = human_response[1],human_response[2]
                    if param == 'test':
                        print('TTTTTTTTTTT test')
                    elif param == 'gamma':
                        self.env_state.hyperparam.gamma = float(value)
                    elif param == 'learning':
                        self.env_state.hyperparam.learning_rate = float(value)
                    elif param == 'exp_max':
                        self.env_state.hyperparam.exploration_max = float(value)
                    elif param == 'exp_decay':
                        self.env_state.hyperparam.exploration_decay = float(value)
                    elif param == 'exp_min':
                        self.env_state.hyperparam.exploration_min = float(value)
                    else:
                        pass




        except:
            traceback.print_exc()
            raise
        finally:
            mutex.release()

    def end(self):
        print("ending...")
        #self.gym_env.close()


if __name__ == "__main__":
    server = GrpcServer(Env, cog_settings)
    server.serve()
