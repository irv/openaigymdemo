# client.py
import cog_settings

from data_pb2 import HumanAction, Config
from cogment.client import Connection

import numpy
import pygame
import video

import os
import sys
import time
import cmd
import pickle
import numpy as np

from pygame.locals import *

MY_ENVIRONMENT = 'CartPole-v1'
#MY_ENVIRONMENT = 'MountainCar-v0'

TRIAL_ENDPOINT = 'orchestrator:9000'

class OAIG_Shell(cmd.Cmd):
    intro = "Continue or quit! Type 'help' to display commands "
    prompt = '(OAIG)'


    def __init__(self):
        super().__init__()
        self.counter = 0
        self.conn_created = False
        self.mouse_image = pygame.image.load('mouse.jpg')

        pygame.font.init()
        #self.myfont = pygame.font.SysFont('Comic Sans MS', 30)
        self.myfont = pygame.font.SysFont('Ariel', 50)

    def do_trialname(self,arg):
        'Set name of trial'
        trial_name = 'X' + arg
        #choice = pickle.dumps(trial_name)
        choice = trial_name
        self.trial.do_action(HumanAction(value=choice))



    def do_params(self,arg):
        'params param:value - where param = gamma,learning,exp_max,exp_min,exp_decay'
        print('AAAAAAA',arg)
        #gamma, learning, exp_max,exp_min,exp_dec
        entry = 'params:' + arg
        print('EEEEEEEEEEE:',entry)
        #choice = pickle.dumps(entry)
        choice = entry
        #self.trial.do_action(Action(value=choice))
        self.trial.do_action(HumanAction(value=choice))


    def do_start(self,arg):
        'Create new trial'
        if self.conn_created:
            print('Trial already exists')
        else:
            self.connection = Connection(cog_settings, TRIAL_ENDPOINT)
            # self.trial = self.connection.start_trial(cog_settings.actor_classes.participant)
            self.trial = self.connection.start_trial(cog_settings.actor_classes.human,
                    Config(env_name = MY_ENVIRONMENT))

            self.conn_created = True

            self.window = pygame.display.set_mode(
                      (self.trial.observation.image_height, self.trial.observation.image_width), 
                      pygame.DOUBLEBUF
                    )

            self.stream = video.StreamReader(
              self.trial.observation.image_width,
              self.trial.observation.image_height
            )



    def do_run(self,arg):
        'Run or Run X, where X is a number'
        if not self.conn_created:
            print('Next time do start to create a trial first')
            self.connection = Connection(cog_settings, TRIAL_ENDPOINT)
            #self.trial = self.connection.start_trial(cog_settings.actor_classes.participant)
            self.trial = self.connection.start_trial(cog_settings.actor_classes.human,
                    Config(env_name = MY_ENVIRONMENT))
            self.conn_created = True

            self.window = pygame.display.set_mode(
                      (self.trial.observation.image_width,self.trial.observation.image_height), 
                      pygame.DOUBLEBUF
                    )

            self.stream = video.StreamReader(
              self.trial.observation.image_width,
              self.trial.observation.image_height
            )

        if "," in arg:
            self.trial_name,max_loop = arg.split(',')
            max_loop = int(max_loop)
            #choice = pickle.dumps('X' + self.trial_name)
            choice = 'X' + self.trial_name
            self.trial.do_action(HumanAction(value=choice))
        elif arg == '':
            max_loop = 1
        else:
            max_loop = int(arg)

        #if arg != '':
        #    max_loop = int(arg)
        self.repeat(max_loop)


    def repeat(self,max_loop):
        i = 0
        stop = False

        while True:


            #print(self.trial.observation.observation)
            if self.trial.observation.env_2_human == 'A_B_done':
                #print('A_B_done')
                #choice = pickle.dumps('do_C')
                #choice = pickle.dumps('X' + self.trial_name)
                choice = 'X' + self.trial_name
                observation = self.trial.do_action(HumanAction(value=choice))
                data = self.stream.decompress(observation.image)
            elif self.trial.observation.env_2_human == 'C_D_done':
                #print('C_D_done')
                #choice = pickle.dumps('do_E')
                choice = 'do_E'
                observation = self.trial.do_action(HumanAction(value=choice))
                data = self.stream.decompress(observation.image)
            elif self.trial.observation.env_2_human == 'E_F_done' and not self.trial.observation.done:
                #print('E_F_done')
                #choice = pickle.dumps('do_E')
                choice = 'do_E'
                observation = self.trial.do_action(HumanAction(value=choice))
                data = self.stream.decompress(observation.image)
            elif  self.trial.observation.env_2_human == 'E_F_done' and self.trial.observation.done:
                i += 1
                print('Run:',i,'Score:',self.trial.observation.score)
                #choice = pickle.dumps('do_nothing')
                choice = 'do_nothing'
                observation = self.trial.do_action(HumanAction(value=choice))
                data = self.stream.decompress(observation.image)

            right_color = (0,0,0)
            left_color = (0,0,0)
            for event in pygame.event.get():
                if event.type == pygame.MOUSEBUTTONDOWN:
                    if event.button == 1: # left click
                        # check for stop here
                        print('left click - push right')
                        #print('In the event loop:', event.pos[0], event.button)
                        self.trial.actors.agent[0].add_feedback(value=1.0, confidence=1.0)
                        left_color = (0,255,0)
                    elif event.button == 3: # right click
                        print('right click - push left')
                        self.trial.actors.agent[0].add_feedback(value=2.0, confidence=1.0)
                        right_color = (0,255,0)


            pygame.pixelcopy.array_to_surface(self.window, data)
            # draw arrows
            pygame.draw.polygon(self.window,left_color,[(20,320),(20,340),(35,330)])
            pygame.draw.polygon(self.window,right_color,[(95,330),(110,320),(110,340)])
            # draw stop button
            #pygame.draw.rect(self.window,(255,0,0),(200,150,100,50))
            # put mouse image on screen
            self.window.blit(self.mouse_image, (40,325))
            textsurface = self.myfont.render('Game:' + str(i) + ' Score: ' + str(self.trial.observation.score),False,(0,0,0))
            self.window.blit(textsurface,(130,355))
            pygame.display.flip()


            if i == max_loop:
                break
        
    def do_end(self, arg):
        'End current trial'
        #choice = pickle.dumps('do_envclose')
        choice = 'do_envclose'
        self.trial.do_action(HumanAction(value=choice))

        # then close trial

        self.trial.end()
        self.conn_created = False

    def do_quit(self, arg):
        'Quit the game'
        if self.conn_created:
            #choice = pickle.dumps('do_envclose')
            choice = 'do_envclose'
            self.trial.do_action(HumanAction(value=choice))

            self.trial.end()
            print('Next time, tear down trial before exiting')
        print('Bye')
        return True


if __name__ == '__main__':
    OAIG_Shell().cmdloop()
