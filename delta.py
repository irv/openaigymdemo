def apply_delta(observation, delta):
    observation.image = delta.image
    observation.pickled_state = delta.pickled_state
    return observation
