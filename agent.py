import cog_settings
from data_pb2 import AgentAction

from cogment import Agent, GrpcServer
import gym
import numpy as np
import time
import random
import pickle
import os
import sys
import DQN2



ENV_NAME = "CartPole-v1"

ITERATIONS = 1000

GAMMA = 0.95
LEARNING_RATE = 0.001

MEMORY_SIZE = 1000000
BATCH_SIZE = 20

EXPLORATION_MAX = 1.0
EXPLORATION_MIN = 0.01
EXPLORATION_DECAY = 0.995


class Agent(Agent):
    VERSIONS = {"agent": "1.0.0"}
    actor_class = cog_settings.actor_classes.agent

    def __init__(self, trial):
        super().__init__(trial)
        self.action_space = None
        self.iterations = 0
        self.feedback = 0

    
    def decide(self, observation):

        if observation.env_2_agent == 'do_B':
            self.dqn_solver = DQN2.DQNSolver(observation.info.observation_space, 
                observation.info.action_space,
                observation.hyperparam.gamma, 
                observation.hyperparam.learning_rate, 
                observation.hyperparam.exploration_min,
                observation.hyperparam.exploration_max, 
                observation.hyperparam.exploration_decay)

            #result = Action()
            #result.value = pickle.dumps('null')
            self.iterations = 0
            #return result
            return AgentAction(pickled_action=pickle.dumps('null'))

        elif observation.env_2_agent == 'do_D':
            self.observation = observation.observation
            self.observation = np.reshape(self.observation, [1, observation.info.observation_space])
            #self.action = self.dqn_solver.act(self.observation)

            push_dir = ['L','R']
            if self.feedback == 1:
                self.action = 1
                #print('HUMAN',push_dir[self.action],self.step)
            elif self.feedback == 2:
                self.action = 0
                #print('HUMAN',push_dir[self.action],self.step)
            else:
                self.action = self.dqn_solver.act(self.observation)


            self.step = 0
            #result = Action()
            #result.value = pickle.dumps(self.action)
            #return result
            return AgentAction(pickled_action=pickle.dumps(self.action))


        elif observation.env_2_agent == 'do_F':
            self.observation_next = observation.observation
            self.observation_next = np.reshape(self.observation_next, [1, observation.info.observation_space])
            self.dqn_solver.remember(self.observation, self.action, observation.reward, self.observation_next, observation.done) 
            self.observation = self.observation_next
            if observation.done:
                self.iterations += 1
                print ("Trial:" + str(observation.trial_name) + ", Run: " + str(self.iterations) + ", exploration: " + str(self.dqn_solver.exploration_rate) + ", score: " + str(self.step))
                #result = Action()
                #result.value = pickle.dumps('null')
                #return result
                return AgentAction(pickled_action=pickle.dumps('null'))
            else:
                self.dqn_solver.experience_replay()
                self.step += 1
                #self.action = self.dqn_solver.act(self.observation)
                # following was above
                push_dir = ['L','R']
                if self.feedback == 1:
                    self.action = 1
                    #print('HUMAN',push_dir[self.action],self.step)
                elif self.feedback == 2:
                    self.action = 0
                    #print('HUMAN',push_dir[self.action],self.step)
                else:
                    self.action = self.dqn_solver.act(self.observation)
                    #print('AGENT',push_dir[self.action] )
                #print('PPPPPPPPPPPPPPP',self.action,self.step)
                #time.sleep(0.5)

                #result = Action()
                #result.value = pickle.dumps(self.action)
                #return result
                return AgentAction(pickled_action=pickle.dumps(self.action))



        elif observation.env_2_agent == 'do_envclose':
                #result = Action()
                #result.value = pickle.dumps('null')
                #return result
                return AgentAction(pickled_action=pickle.dumps('null'))


        elif observation.env_2_agent == 'do_nothing':
                #result = Action()
                #result.value = pickle.dumps('null')
                #return result
                return AgentAction(pickled_action=pickle.dumps('null'))

    def reward(self, reward):
        self.feedback = int(reward.value)
        pass


    def end(self):
        pass


if __name__ == '__main__':
    server = GrpcServer(Agent, cog_settings)
    server.serve()