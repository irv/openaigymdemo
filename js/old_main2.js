import {Connection} from 'cogment';
import cog_settings from './cog_settings';

const pako = require('pako');
const protos = require("./data_pb")




function display(observation) {
  //console.log(observation.toObject())

  const w = observation.getImageWidth();
  const h = observation.getImageHeight();

  var canvas = document.querySelector('#render');
  canvas.width = w;
  canvas.height = h;


  var ctx = canvas.getContext("2d");
  ctx.fillStyle = "blue";
  ctx.fillRect(0, 0, canvas.width, canvas.height);

  
  var img_data = pako.inflate(observation.getImage());
  // console.log(img_data)

  var image_data = new Uint8ClampedArray(w * h * 4)


  for(var i = 0 ; i < w; ++i) {
    for(var j = 0 ; j < h; ++j) {
      const index = i * h + j;
      image_data[index * 4] = img_data[index*3];
      image_data[index * 4 + 1] = img_data[index*3 + 1];
      image_data[index * 4 + 2] = img_data[index*3 + 2];
      image_data[index * 4 + 3 ] = 255;
    }
  }

  var image = new ImageData(image_data, w, h);
  //console.log(image.data)

  ctx.putImageData(image, 0, 0);
}


function showmesg(t, form) {

   let area = document.forms[form + 'form'].getElementsByTagName('textarea')[0];

   area.value += t + '\n';
   area.scrollTop = area.scrollHeight;
} // end of showmesg



async function repeat(max_loop,trial,obs,trial_name) {
  var i = 0;

  while (true) {

    if (obs.getEnv2Human() == 'A_B_done') {
      var choice = 'X' + trial_name
      var action = new protos.HumanAction();
      action.setValue(choice);
      obs = await trial.do_action(action);  // !!!!!!!!!!!!  this may be a problem going var obs =
      display(obs);
    }

    else if (obs.getEnv2Human() == 'C_D_done') {
      var choice = 'do_E';
      var action = new protos.HumanAction();
      action.setValue(choice);
      obs = await trial.do_action(action);  // !!!!!!!!!!!!  this may be a problem going var obs =
      display(obs);
    }

    else if ( (obs.getEnv2Human() == 'E_F_done') && !(obs.getDone())) {
      var choice = 'do_E';
      var action = new protos.HumanAction();
      action.setValue(choice);
      obs = await trial.do_action(action);  // !!!!!!!!!!!!  this may be a problem going var obs =
      display(obs);
    }

    else if ( (obs.getEnv2Human() == 'E_F_done') && (obs.getDone())) {
      i++
      showmesg("Run: " + i + ', Score: ' + obs.getScore(),'test');

      var choice = 'do_nothing';
      var action = new protos.HumanAction();
      action.setValue(choice);
      obs = await trial.do_action(action);  // !!!!!!!!!!!!  this may be a problem going var obs =
      display(obs);



    }

    // here you might want to print some stuff like in python version
    if (i == max_loop) {
      break;
    }
  } // end of while true

  return false;
} // end of repeat


async function launch() {

  var counter = 0;
  var conn_created = false;
  // self.mouse_image = pygame.image.load('mouse.jpg')
  var run_btn = document.querySelector("#run");
  var end_btn = document.querySelector("#end");
  var right_btn = document.querySelector('#right');
  var left_btn = document.querySelector('#left');
  var games = document.querySelector('#games');
  var t_name = document.querySelector('#t_name');
  var rightleft_btn = document.querySelector('#rightleft');
  
  var aom_conn = new Connection(cog_settings, "http://127.0.0.1:8088")
  //var aom_conn = new Connection(cog_settings,"http://" + window.location.hostname + ":8088")
  //var aom_conn = new Connection(cog_settings, "http://3.19.169.7:8088")


  var trial = undefined;
  var ready = false;
  var running = false;
  var obs = undefined;

  run_btn.onclick = async function() {
    if (!conn_created) {
      var cfg = new protos.Config();
      cfg.setEnvName('CartPole-v1');
      //cfg.setEnvName('MountainCar-v0');
      trial = await aom_conn.start_trial(cog_settings.actor_classes.human, cfg);
      //ready = true;
      conn_created = true;
      display(trial.observation);
    }   

    var max_loop = games.value
    var trial_name = t_name.value

    if (games.value == '') max_loop = 1; 
    if (trial_name == '') trial_name = "un-named"

    showmesg("Running trial - " + trial_name,'test');

    var choice = 'X' + trial_name

    var action = new protos.HumanAction();

    action.setValue(choice);

    obs = await trial.do_action(action);  // !!!!!!!!!!!!  this may be a problem going var obs =
    //console.log(trial)

    conn_created = await repeat(max_loop,trial,obs,trial_name);

    choice = 'do_envclose'
    action.setValue(choice);
    obs = await trial.do_action(action);  // !!!!!!!!!!!!  this may be a problem going var obs =
    trial.end();
    trial = undefined;
    showmesg("Trial over, try again",'test');


  };



  right_btn.onclick = async function() {
    showmesg("<",'test');
    trial.actors.agent[0].add_feedback(2.0, 1.0)  // value, confidence

  };

  left_btn.onclick = async function() {
    showmesg(">",'test');
    trial.actors.agent[0].add_feedback(1.0, 1.0);  // value, confidence
  };

  rightleft_btn.onmousedown = async function(event) {
    if (event.button == 0) {
      if (event.clientX < screen.width/2) {
        showmesg("left side",'test');
      }
      else {
        showmesg("right side",'test')
      }

      trial.actors.agent[0].add_feedback(2.0, 1.0);  // value, confidence

      //showmesg("<< " + event.clientX + " " + screen.width,'test');
    }
    if (event.button == 2) {
      trial.actors.agent[0].add_feedback(1.0, 1.0);  // value, confidence
      showmesg(">>",'test');
    }
  };



}

// following turns of right button inspect in browser
window.oncontextmenu = function () {
   return false;
}
document.onkeydown = function (e) { 
    if (window.event.keyCode == 123 ||  e.button==2)    
    return false;
}

// need to set following for order of things to happen properly
window.addEventListener("DOMContentLoaded", function() {
        console.log("Dom loaded");
        launch();
    }, false);


// window.setTimeout(launch, 1000);


